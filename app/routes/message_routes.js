module.exports = (app, db) => {
    app.get('/messages', (req, res) => {
        let query = db.collection('messages');

        const date = req.query.datetime;
        if (date) {
            const dateObj = new Date(date);
            if (isNaN(dateObj.getDate())) {
                return res.send({'error': 'Invalid date'});
            }

            query = query.find({'datetime': {$gt: dateObj}});
        } else {
            query = query.find();
        }

        query
            .sort({datetime: -1})
            .limit(30)
            .toArray()
            .then(function(data) {
                res.send(data.reverse());
            })
            .catch(function(err) {
                res.send({'error': 'An error has occured'});
            });
    });

    app.post('/messages', (req, res) => {
        const message = req.body.message;
        const author = req.body.author;
        if (!message || !author) {
            return res.send({'error': 'Message or author should be populated'});
        }
        const messageObject = { message, author, datetime: new Date() };
        db.collection('messages').insertOne(messageObject).then((result) => {
            res.send(result.ops[0]);
        }).catch((err) => {
            res.send({ 'error': 'An error has occurred' });
        });
    });
};