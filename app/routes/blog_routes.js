module.exports = (app, db) => {
    const findUser = function (email) {
        return db.collection('users').findOne({email});
    };

    const createUser = function(email) {
        return db.collection('users').insertOne({email, firstName: 'John', lastName: 'Doe'});
    };

    const updateUser = function(user, firstName, lastName) {
        return db.collection('users').findOneAndUpdate(user,
            {$set: {firstName, lastName}},
            {returnOriginal: false})
            .then((result) => result.value);
    };

    const findOrCreateUser = function(email) {
        return findUser(email).then(function(user) {
            return user || createUser(email).then((result) => result.ops[0]);
        });
    };

    const findExistingUser = function(email) {
        return findUser(email).then(function(user) {
            if (user) return user;
            throw new Error('User not found. Please create user by GET /profile first');
        });
    };

    const tryToUpdateUser = function(request) {
        return function(user) {
            const firstName = request.firstName;
            const lastName = request.lastName;
            if (!firstName || !lastName) {
                throw new Error('Request should have "firstName" and "lastName" values!');
            }

            return updateUser(user, firstName, lastName);
        }
    };

    const tryToCreatePost = function(request) {
        return function(user) {
            const message = request.message;
            if (!message) {
                throw new Error('Request should have "message" value!');
            }

            const newPost = {userId: user._id, message, datetime: new Date()};
            return db.collection('blog_posts').insertOne(newPost).then((result) => result.ops[0]);
        };
    };

    const findPosts = function(datetime) {
        return function(user) {
            let $match = {userId: user._id};

            if (datetime) {
                const date = new Date(datetime);
                if (isNaN(date.getDate())) {
                    throw new Error('Invalid "datetime" string');
                }

                $match.datetime = {$gt: date};
            }

            if (user.subscriptions) {
                let userIds = [user._id].concat(user.subscriptions);
                $match.userId = {$in: userIds};
            }

            return db.collection('blog_posts').aggregate([
                {$match},
                {$sort: {datetime: -1}},
                {$limit: 30},
                {
                    $lookup: {
                        'from': 'users',
                        'localField': 'userId',
                        'foreignField': '_id',
                        'as': 'user'
                    }
                },
                {$unwind: "$user"},
            ]).toArray().then((posts) => posts.reverse());
        };
    };

    const tryToAddSubscription = function(request) {
        return function(user) {
            const email = request.email;
            if (!email) throw new Error('"email" field should not be empty!');

            return findUser(email).then(function(subUser) {
                if (!subUser) throw new Error('This user does not exist, cannot subscribe');

                return db.collection('users')
                    .findOneAndUpdate(user, {$addToSet: {subscriptions: subUser._id}}, {returnOriginal: false});
            }).then((result) => result.value);
        }
    };

    const removeSubscriptions = function(user) {
        return db.collection('users')
            .findOneAndUpdate(user, {$set: {subscriptions: []}}, {returnOriginal: false})
            .then((result) => result.value);
    };

    const findSubscriptions = function(user) {
        return db.collection('users')
            .find({_id: {$in: user.subscriptions}})
            .toArray();
    };

    app.get('/blog/:email/profile', (req, res) => {
        findOrCreateUser(req.params.email)
            .then((user) => res.send(user))
            .catch((error) => res.send({error: error.message}));
    });

    app.post('/blog/:email/profile', (req, res) => {
        findExistingUser(req.params.email)
            .then(tryToUpdateUser(req.body))
            .then((user) => res.send(user))
            .catch((error) => res.send({error: error.message}));
    });

    app.post('/blog/:email/posts', (req, res) => {
        findExistingUser(req.params.email)
            .then(tryToCreatePost(req.body))
            .then((post) => res.send(post))
            .catch((error) => res.send({error: error.message}));
    });

    app.get('/blog/:email/posts', (req, res) => {
        findExistingUser(req.params.email)
            .then(findPosts(req.query.datetime))
            .then((posts) => res.send(posts))
            .catch((error) => res.send({error: error.message}));
    });

    app.post('/blog/:email/subscribe', (req, res) => {
        findExistingUser(req.params.email)
            .then(tryToAddSubscription(req.body))
            .then((subscription) => res.send(subscription))
            .catch((error) => res.send({error: error.message}));
    });

    app.post('/blog/:email/subscribe/delete', (req, res) => {
        findExistingUser(req.params.email)
            .then(removeSubscriptions)
            .then((user) => res.send(user))
            .catch((error) => res.send({error: error.message}));
    });

    app.get('/blog/:email/subscribe', (req, res) => {
        findExistingUser(req.params.email)
            .then(findSubscriptions)
            .then((subscriptions) => res.send(subscriptions))
            .catch((error) => res.send({error: error.message}));
    });
};